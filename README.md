# React Sandbox

React Hands-on


# Overview of React Starter Projects
http://andrewhfarmer.com/starter-project/

# Further Links

There’s also a <a href="https://vimeo.com/168648012">fantastic video</a> on how
to structure your React.js apps with scalability in mind. It provides rationale
for the majority of boilerplate's design decisions.
(https://github.com/mxstbr/react-boilerplate)

